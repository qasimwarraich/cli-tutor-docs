# CLI-Tutor

```txt
        _ _       __        __
  _____/ (_)     / /___  __/ /_____  _____
 / ___/ / /_____/ __/ / / / __/ __ \/ ___/
/ /__/ / /_____/ /_/ /_/ / /_/ /_/ / /
\___/_/_/      \__/\__,_/\__/\____/_/

A simple command line tutor application that aims to introduce users to the
    basics of command line interaction.
    Web version is available at https://clitutor.chistole.ch

```

[![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/qasimwarraich/cli-tutor/latest?label=docker)](https://hub.docker.com/r/qasimwarraich/cli-tutor)
[![Website](https://img.shields.io/website?label=web%20version&up_color=light%20green&up_message=live&url=https%3A%2F%2Ftutor.chistole.ch)](https://tutor.chistole.ch)

## What is this?

Despite the arguably dated appearance, difficult learning curve and practical
non-existence in the general personal computing space, Command Line Interfaces
(CLIs) have more than stood the test of time in the software development world.
There are a multitude of extremely popular tools and applications that
primarily focus on the command line as an interaction medium. Some examples
include version control software like `git`, compilers and interpreters for
programming languages, package managers and various core utilities that are
popular in areas such as software development, scripting and system
administration.

As mentioned before, the use of the command line as an interaction paradigm has
effectively disappeared from a mainstream personal computer usage perspective.
This contributes greatly to the intimidation factor and learning difficulty for
those interested in getting into software engineering or system administration.
This unfamiliarity, paired with the inevitability of usage of CLIs in the
development space highlights a need to make the command line more accessible to
new users for whom text-based interaction with their computer is an alien
concept. In recent years, interactive learning aides utilising tools such as
sandboxed environments have been gaining in popularity and have the potential
to be a suitable medium for learning command line basics through actual usage,
examples and practice.

This web documentation serves as a companion to the
[cli-tutor](https://gitlab.com/qasimwarraich/cli-tutor) command line
application being developed as part of a Master's thesis project at The
University of Zurich. The purpose of this web view is to provide a comparison
point between interactive and non-interactive learning mediums.

### Have Fun!
