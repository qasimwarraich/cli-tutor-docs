# Welcome To CLI-Tutor

In the following lessons, you will read and learn about some fundamentals of
command line interaction. The lessons are written from the perspective of a
follow-along guide. Feel free to open up a terminal on your computer and follow
along.


???+ info
    If you do not have access to a terminal or you don't want to use your own
    system. Feel free to follow this link below and spawn a Linux terminal.

    [![Website](https://img.shields.io/website?label=web%20terminal&up_color=light%20green&up_message=live&url=https%3A%2F%2Fcli.chistole.ch)](https://cli.chistole.ch){target=_blank}
    ???+ note
            Sometimes the terminal might not clear its loading text. You can
            just press enter and proceed 😇.

All the best!
